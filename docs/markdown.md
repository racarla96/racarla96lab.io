---
id: Markdown
title: Markdown Tips
sidebar_label: Markdown 
---

## Markdown and Unicode (UTF-8)

Sometimes do you want to write some equations in Markdown. Docusaurus, the framework that uses this site. it is using markdown for documentation and have some funcionalities as support superscript and subscript, but if you want to write a strange character like this: &#373; for write equations is difficult. But, markdown and html support UTF-8, so you can search the special character code for in the [fileformat unicode list](http://www.fileformat.info/info/charset/UTF-8/list.htm) and write it in your markdown document easily.

[Unicode in Markdown](https://stackoverflow.com/questions/34538879/unicode-in-github-markdown/36616878)
[Fileformat unicode list](http://www.fileformat.info/info/charset/UTF-8/list.htm)